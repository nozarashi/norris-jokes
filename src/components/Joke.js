import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import classNames from 'classnames';

const Joke = () => {
  const [joke, setJoke] = useState({});
  const [category, setCategory] = useState('');
  const [categories, setCategories] = useState([]);
  const [copied, setCopied] = useState(false);
  const jokeRef = useRef(null);

  async function getJoke() {
    let url = 'https://api.chucknorris.io/jokes/random';
    if (category !== '') {
      url += `?category=${category}`;
    }
    const res = await axios.get(url);
    setJoke(res.data);
  }

  async function getCategories() {
    const res = await axios.get('https://api.chucknorris.io/jokes/categories');
    setCategories(res.data);
  }

  const copyToClipboard = () => {
    const jokeParagraph = jokeRef.current;
    const range = new Range();
    range.setStart(jokeParagraph, 0);
    range.setEnd(jokeParagraph, 1);
    document.getSelection().removeAllRanges();
    document.getSelection().addRange(range);
    document.execCommand('copy');
    document.getSelection().removeAllRanges();
    setCopied(true);
    setTimeout(() => setCopied(false), 2000);
  };

  useEffect(() => {
    getJoke();
    getCategories();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <div className="w-64 mb-4 mx-auto">
        <label htmlFor="category" className="block tracking-wide font-semibold">
          Select a category
        </label>
        <div className="inline-block relative w-64 ">
          <select
            id="category"
            className="input-select w-full"
            value={category}
            onChange={(e) => setCategory(e.target.value)}
          >
            <option value="">No category</option>
            {categories.map((categoryItem) => (
              <option key={categoryItem} value={categoryItem}>
                {categoryItem}
              </option>
            ))}
          </select>
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <svg
              className="fill-current h-4 w-4"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
            >
              <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
            </svg>
          </div>
        </div>
      </div>

      <div className="flex flex-wrap lg:w-2/3 w-full mx-auto px-4">
        <p id="joke-value" className="text-xl leading-relaxed" ref={jokeRef}>
          {joke.value}
        </p>
      </div>

      <div className="w-full md:w-1/2 mx-auto relative mt-8 flex flex-wrap justify-center px-4">
        <button
          className="btn btn-primary uppercase mr-2"
          onClick={(e) => getJoke()}
        >
          Random joke
        </button>
        <button
          className="btn bg-gray-400 text-gray-800"
          title="Copy to clipboard"
          onClick={copyToClipboard}
        >
          <i className="fas fa-copy" />
        </button>
        <div
          className={classNames(
            'absolute bg-gray-800 text-white rounded-full px-2 py-1 text-xs font-bold',
            {
              'slow-hidden': !copied,
            }
          )}
          style={{
            top: 5,
            right: 5,
          }}
        >
          Copied
        </div>
      </div>
    </>
  );
};

export default Joke;
