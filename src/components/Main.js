import React from 'react';
import logo from '../img/logo.jpg';
import Joke from './Joke';

const Main = () => {
  return (
    <div className="container mx-auto mt-8 lg:mt-16 text-gray-800">
      <img src={logo} alt="Chuck Norris" className="block h-48 mx-auto" />
      <div className='mb-4'>
        <h1 className="bungee-inline text-center text-4xl tracking-wide text-orange-600">
          Norris Jokes
        </h1>
      </div>
      <Joke />
    </div>
  );
};

export default Main;
